<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Language</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('language', $languages , null , ['class' => 'form-control','placeholder'=>'Select Language','id'=>'language','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Title</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>'Enter Title','autocomplete'=>'off','id'=>'title','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Image</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::file('image', ['class' => 'form-control','accept'=>'.png','required']) !!}
        </div>
    </div>
</div>

