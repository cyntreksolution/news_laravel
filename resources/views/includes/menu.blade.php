<ul class="menu-nav">
    <li class="menu-item menu-item-open menu-item-here menu-item-rel "
        data-menu-toggle="click" aria-haspopup="true">
        <a href="{{route('dashboard')}}" class="menu-link ">
            <span class="menu-text">Dashboard</span>
            <i class="menu-arrow"></i>
        </a>
    </li>

    <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click"
        aria-haspopup="true">
        <a href="javascript:;" class="menu-link menu-toggle">
            <span class="menu-text">Configurations</span>
            <span class="menu-desc"></span>
            <i class="menu-arrow"></i>
        </a>

        <div class="menu-submenu menu-submenu-classic menu-submenu-left">
            <ul class="menu-subnav">
                <li class="menu-item" aria-haspopup="true">
                    <a href="#" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">Language</span>
                    </a>
                </li>
                <li class="menu-item" aria-haspopup="true">
                    <a href="{{route('categories.index')}}" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">Category</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

    <li class="menu-item"
        data-menu-toggle="click" aria-haspopup="true">
        <a href="#" class="menu-link ">
            <span class="menu-text">Posts</span>
            <i class="menu-arrow"></i>
        </a>
    </li>

</ul>
