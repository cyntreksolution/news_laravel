<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Institute;
use App\Models\Language;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::pluck('language', 'id');
        return view('category.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $path = env('IMAGE_DIR');
        $destinationPath = $path;

        $type = $file->getClientMimeType();

        $fileName = md5(uniqid());
        $fileName = $fileName . '.png';
        $file->move($destinationPath, $fileName);
        $image_name = $file->getClientOriginalName();

        $media = new Media();
        $media->titre = $image_name;
        $media->url = $fileName;
        $media->type = $type;
        $media->extension = '.png';
        $media->date = Carbon::now()->format('Y-m-d H:i:s');
        $media->enabled = 1;
        $media->save();

        $cat = Category::orderBy('id', 'DESC')->first();


        $category = new Category();
        $category->title = $request->title;
        $category->language_id = $request->language;
        $category->media_id = $media->id;
        $category->position = !empty($cat) && $cat->count() > 0 ? $cat->id + 1 : 0;
        $category->save();

        $msg = 'Category Created Successfully';
        return $this->sendResponse($category, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $file = $request->file('image');
        if (!empty($file)) {
            $path = env('IMAGE_DIR');
            $destinationPath = $path;

            $type = $file->getClientMimeType();

            $fileName = md5(uniqid());
            $fileName = $fileName . '.png';
            $file->move($destinationPath, $fileName);
            $image_name = $file->getClientOriginalName();

            $media = new Media();
            $media->titre = $image_name;
            $media->url = $fileName;
            $media->type = $type;
            $media->extension = '.png';
            $media->date = Carbon::now()->format('Y-m-d H:i:s');
            $media->enabled = 1;
            $media->save();
        }


        $category->title = $request->title;
        $category->language_id = $request->language;
        if (!empty($file)) {
            $category->media_id = $media->id;
        }
        $category->save();

        $msg = 'Category Updated Successfully';
        return $this->sendResponse($category, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return $this->sendResponse('', 'Category Successfully Deleted');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'language_table.language', 'title'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Category::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Category::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
//        $can_edit = ($user->hasPermissionTo('institute edit')) ? 1 : 0;
        $can_edit = 1;
//        $can_delete = ($user->hasPermissionTo('institute delete')) ? 1 : 0;
        $can_delete = 1;

        foreach ($dataset as $key => $item) {
            $image = $item->image->url;
            $path = env('IMAGE_DIR');
            $img_url = $path . '/' . $image;
            if ($can_edit) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-title='{$item->title}' data-language='{$item->language_id}' data-image='{$img_url}'></i>";
            }
            if ($can_delete) {
                $url = "'categories/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }


            $img = "<img class='img img-thumbnail' src='$image'>";


            $data[$i] = array(
                $item->id,
                $item->language()->first()->language,
                $item->title,
                $img,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
