<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    public $timestamps=false;
    protected $table ='category_table';

    public function language(){
        return $this->belongsTo(Language::class,'language_id');
    }

    public function image(){
        return $this->belongsTo(Media::class,'media_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('language_table.language','category_table.*')
            ->join('language_table', 'category_table.language_id', '=', 'language_table.id')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeFilterData($query, $promotion, $channel, $outlet, $user, $date)
    {
        if (!empty($promotion)) {
            $query->where('id', '=', $promotion);
        }
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('institutes.id', 'like', "%" . $term . "%")
            ->orWhere('institutes.name', 'like', "%" . $term . "%")
            ->orWhereHas('owner', function ($q) use ($term) {
                $q->where('name', 'like', "%" . $term . "%");
            })
            ->orWhere('address', 'like', "%" . $term . "%")
            ->orWhere('telephone', 'like', "%" . $term . "%")
            ->orWhereHas('cities', function ($q) use ($term) {
                $q->where('name_en', 'like', "%" . $term . "%");
            });
    }
}
