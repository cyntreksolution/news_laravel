FormOptions = {
    submitForm: function (form_id, modal_id, table_id) {
        form_id = '#' + form_id;
        table_id = '#' + table_id;
        let url = $(form_id).attr('action');
        let method = $(form_id).attr('method');

        $(form_id).ajaxSubmit(
            {
                clearForm: true,
                url: url,
                type: method,
                success: function (result) {
                    ModalOptions.hideModal(modal_id);
                    if (result.success) {
                        let table = $(table_id).DataTable();
                        table.ajax.reload();
                        Notifications.showSuccessMsg(result.message);
                    } else {
                        Notifications.showErrorMsg(result.message);
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    ModalOptions.hideModal(modal_id);
                    Notifications.showErrorMsg(errorThrown);
                }
            }
        );

    },


    deleteRecord: function (record_id, url, table) {
        let tableName = '#' + table;
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    data: {
                        id: record_id,
                    },
                    success: function (result) {
                        if (result.success) {
                            let table = $(tableName).DataTable();
                            table.ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                'Your record has been deleted.',
                                'success'
                            )
                        } else {
                            let msg = result.message;
                            Notifications.showErrorMsg(msg);
                        }
                    }
                });
            }
        })
    }

}
