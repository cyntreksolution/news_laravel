const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.copyDirectory('resources/css', 'public/css');
mix.copyDirectory('resources/js', 'public/js');
mix.copyDirectory('resources/plugins', 'public/plugins');
mix.copyDirectory('resources/media', 'public/media');

mix.js('resources/js/custom/notifications.js', 'public/js');
mix.js('resources/js/custom/modal.js', 'public/js');
mix.js('resources/js/custom/dataTable.js', 'public/js');
mix.js('resources/js/custom/FormOptions.js', 'public/js');
mix.copy('resources/plugins/global/jquery.form.js', 'public/js');

